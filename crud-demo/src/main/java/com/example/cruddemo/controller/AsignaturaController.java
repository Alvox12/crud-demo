package com.example.cruddemo.controller;


import com.example.cruddemo.model.Asignatura;
import com.example.cruddemo.model.Persona;
import com.example.cruddemo.service.AsignaturaServiceAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/asignatura/api/v1")
@CrossOrigin("*")
public class AsignaturaController {

    @Autowired
    private AsignaturaServiceAPI asignaturaServiceAPI;

    @GetMapping(value="/all")
    public List<Asignatura> getAll() {
        return asignaturaServiceAPI.getAll();
    }

    @GetMapping(value = "/find/{id}")
    public Asignatura find(@PathVariable Long id) {
        return asignaturaServiceAPI.get(id);
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Asignatura> save(@RequestBody Asignatura asignatura) {
        Asignatura obj = asignaturaServiceAPI.save(asignatura);
        return new ResponseEntity<Asignatura>(obj, HttpStatus.OK);
    }

    @GetMapping(value = "/delete/{id}")
    public ResponseEntity<Asignatura> delete(@PathVariable Long id) {
        Asignatura asignatura = asignaturaServiceAPI.get(id);
        if(asignatura != null) {
            asignaturaServiceAPI.delete(id);
        } else {
            return new ResponseEntity<Asignatura>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Asignatura>(asignatura, HttpStatus.OK);
    }

}
