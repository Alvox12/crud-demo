package com.example.cruddemo.controller;

import com.example.cruddemo.model.Asignatura;
import com.example.cruddemo.model.Persona;
import com.example.cruddemo.service.AsignaturaServiceAPI;
import com.example.cruddemo.service.PersonaServiceAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/persona/api/v1")
@CrossOrigin("*")
public class PersonaController {

    @Autowired
    private PersonaServiceAPI personaServiceAPI;

    @Autowired
    private AsignaturaServiceAPI asignaturaServiceAPI;


    @GetMapping(value="/all")
    public List<Persona> getAll() {
        return personaServiceAPI.getAll();
    }

    @GetMapping(value = "/find/{id}")
    public Persona find(@PathVariable Long id) {
        return personaServiceAPI.get(id);
    }

    @GetMapping(value = "/asignatura/all/{id}")
    public List<Asignatura> findUserAsgs(@PathVariable Long id) {
        List<Long> ids = personaServiceAPI.get(id).getAsignaturasIds();
        Asignatura asg;
        List<Asignatura> userAsgs = new ArrayList<Asignatura>();

        for (Long asgId : ids) {
           asg = asignaturaServiceAPI.get(asgId);
           if(asg != null)
               userAsgs.add(asg);
        }

        return userAsgs;
    }

    @GetMapping(value = "/asignatura/find/{id}/{asgId}")
    public boolean cursaAsg(@PathVariable Long id, @PathVariable Long asgId) {
        List<Long> ids = personaServiceAPI.get(id).getAsignaturasIds();
        return ids.contains(asgId);
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Persona> save(@RequestBody Persona persona) {
        Persona obj = personaServiceAPI.save(persona);
        return new ResponseEntity<Persona>(obj, HttpStatus.OK);
    }

    @GetMapping(value = "/delete/{id}")
    public ResponseEntity<Persona> delete(@PathVariable Long id) {
        Persona persona = personaServiceAPI.get(id);
        if(persona != null) {
            personaServiceAPI.delete(id);
        } else {
            return new ResponseEntity<Persona>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Persona>(persona, HttpStatus.OK);
    }

    @PostMapping(value = "/asignatura/add/{id}")
    public ResponseEntity<Asignatura> addAsg(@PathVariable Long id, @RequestBody Asignatura asignatura) {

        //Si asignatura NO existe en base de datos se agrega
        if(asignaturaServiceAPI.get(asignatura.getId()) == null) {
            asignaturaServiceAPI.save(asignatura);
        }

        Persona p1 = personaServiceAPI.get(id);
        List<Long> ids = p1.getAsignaturasIds();

        //Si persona ya cursa esa asignatura no se hace nada
        if(ids.contains(asignatura.getId())){
            return new ResponseEntity<Asignatura>(HttpStatus.OK);
        }

        //Se añade asignatura a listado de ids de persona
        p1.addAsignaturaId(asignatura.getId());
        //Se actualiza persona con su nueva asignatura
        personaServiceAPI.save(p1);

        return new ResponseEntity<Asignatura>(asignatura, HttpStatus.OK);
    }

}
