package com.example.cruddemo.repository;

import com.example.cruddemo.model.Asignatura;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AsignaturaRepository extends MongoRepository<Asignatura, Long> {

}
