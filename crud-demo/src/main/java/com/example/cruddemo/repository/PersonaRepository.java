package com.example.cruddemo.repository;

import com.example.cruddemo.model.Persona;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonaRepository extends MongoRepository<Persona, Long> {

}
