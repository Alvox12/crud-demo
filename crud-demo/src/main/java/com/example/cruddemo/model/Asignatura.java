package com.example.cruddemo.model;

import org.springframework.data.annotation.Id;

public class Asignatura {

    @Id
    private Long id;

    private String nombre;
    private String descripcion;
    private int creditos;
    private int numEstudiantes;

    public Asignatura() {

    }

    public Asignatura(Long id, String nombre, String descripcion, int creditos, int numEstudiantes) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.creditos = creditos;
        this.numEstudiantes = numEstudiantes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public int getNumEstudiantes() {
        return numEstudiantes;
    }

    public void setNumEstudiantes(int numEstudiantes) {
        this.numEstudiantes = numEstudiantes;
    }
}
