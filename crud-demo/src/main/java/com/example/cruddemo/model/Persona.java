package com.example.cruddemo.model;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Persona {

    @Id
    private Long id;

    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;
    
    private List<Long> asignaturasIds;

    public Persona() {

    }

    public Persona(Long id, String nombre, String apellido, String direccion, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.asignaturasIds = new ArrayList<Long>();
    }

    public Persona(Long id, String nombre, String apellido, String direccion, String telefono, List<Long> asignaturasIds) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.asignaturasIds = asignaturasIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<Long> getAsignaturasIds() {
        return asignaturasIds;
    }

    public void setAsignaturasIds(List<Long> asignaturasIds) {
        this.asignaturasIds = asignaturasIds;
    }

    public void addAsignaturaId(Long id) {
        if(this.asignaturasIds == null)
            this.asignaturasIds = new ArrayList<>();

        this.asignaturasIds.add(id);
    }

}
