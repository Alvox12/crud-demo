package com.example.cruddemo.service;

import com.example.cruddemo.commons.GenericServiceAPI;
import com.example.cruddemo.model.Persona;

public interface PersonaServiceAPI extends GenericServiceAPI<Persona, Long> {

}
