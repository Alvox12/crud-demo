package com.example.cruddemo.service;

import com.example.cruddemo.commons.GenericServiceImpl;
import com.example.cruddemo.model.Asignatura;
import com.example.cruddemo.repository.AsignaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class AsignaturaServiceImpl extends GenericServiceImpl<Asignatura, Long> implements AsignaturaServiceAPI{

    @Autowired
    private AsignaturaRepository asignaturaRepository;

    @Override
    public CrudRepository<Asignatura, Long> getDao() {
        return asignaturaRepository;
    }
}
