package com.example.cruddemo.service;

import com.example.cruddemo.commons.GenericServiceAPI;
import com.example.cruddemo.model.Asignatura;

public interface AsignaturaServiceAPI extends GenericServiceAPI<Asignatura, Long> {

}
